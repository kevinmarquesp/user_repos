defmodule UserRepos do
  @moduledoc """
  Módulo idiota que usei pra me familiarizar um pouco com a
  syntaxe da linguagem Elixir. Acho que fica clasro o quão iniciante eu sou
  nessa linguagem por não estar seguindo nenhum guia de estilos

  ## Coisas legais que aprendi nessa brincadeira
  1. Docstrings (em markdown) em cima das funções, com espacços em branco depois
     do `"""`
  2. A syntaxe de pipe (`|>`) é mais poderosa do que parece
  3. Não dá pra compilar pra um ELF da vida, ao menos não sem ainda depender do
     erlang, o que mostra como Elixir é uma linguagem diferente e não devo
     aplicar ela em qualquer lugar sem pensar
  4. Esse projeto chega a ser uma ofença de tão simples que ele é
  5. Oh my god, corzinha UwU
  """

  @doc """
  A função que deve ser a mais feia em comparação com as outras, ela só
  mostra as informações de cada repo na tela iterando pela lista de repositŕios
  
  Funções anonimas sempre serão uma feature legal e, olha, corzinha UwU
  """
  def views_show_values repos do
    Enum.each repos, fn repo ->
      IO.puts "-------------------------"

      IO.ANSI.format([:blue, "Project name: ", :white, repo["name"]])
      |> IO.puts()

      if repo["description"] != nil do
        IO.ANSI.format([:blue, "Description: ", :white, repo["name"]])
        |> IO.puts()
      end

      if repo["fork"] do
        IO.ANSI.format([:green, "Forketd!"])
        |> IO.puts()
      end

      IO.puts "-------------------------\n"
    end
  end

  @doc """
  Então, isso aqui é o feijão com arroz, e a syntaxe de uma função com
  dois retornos lembra muito a do Pyhton ou Lua, nada de mais aqui

  Apesar disso, não sei se o uso desse `if` no final é uma boa prática, pois já
  li em algum lugar que o `if` não é usado com frequência pelos programadores de
  Elixir, não faço ideia do porque
  """
  def api_get_repos user do
    HTTPoison.start
    {status, result} = JSON.decode HTTPoison.get!("https://api.github.com/users/#{user}/repos").body
    if status == :ok do
      result
    end
  end

  @doc """
  Primeira vez usando um módulo externo, o optimus é fácil de enteder e
  é bem documentado. Essa função so configurar as opções e flags dados pela
  linha de comando e retorna um mapa já com esses argumentos parseados

  Apropósito, essa syntaxe do retorno da função ser a última linha dela é
  genial, juntamente com essas funções de linha úniica
  """
  def optimus_setup(args), do: Optimus.new!(
    name: "userrepos",
    description: "Stupid CLI application to show pinned repos from Github users",
    version: "0.1.0",
    allow_unknown_args: false,
    parse_double_dash: true,
    options: [
      user: [
        value_name: "USER",
        long: "--user",
        short: "-u",
        help: "User to display the repos"
      ]
    ]
  ) |> Optimus.parse!(args)

  @doc """
  Não tinha muita ideia do que colocar aqui, mas pareceu fazer sentido
  passar os argumentos já parseados como um parametro para função principal
  """
  def main args do
    api_get_repos(args.options.user)
    |> views_show_values()
  end
end

# Mais uma vez, não faço ideia se isso é uma boa prática
System.argv()
|> UserRepos.optimus_setup()
|> UserRepos.main()
